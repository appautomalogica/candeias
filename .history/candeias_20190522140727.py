## pip install flask-login
#import web
## pip install pyOpenSSL 

import os
import json
import pyodbc
import datetime
from OpenSSL import SSL
from flask import Flask, Response, redirect, url_for, request, session, abort
from flask_login import LoginManager, UserMixin, \
                                login_required, login_user, logout_user 
from flask import send_from_directory
from flask import request
from flask import send_file
from flask_cors import CORS, cross_origin

from datetime import date, datetime

def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError ("Type %s not serializable" % type(obj))

app = Flask(__name__)
CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

# config
app.config.update(
    DEBUG = False,
    SECRET_KEY = 'secret_xxx'
)

# flask-login
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

# silly user model
class User(UserMixin):

    def __init__(self, id):
        self.id = id
        self.name = "user" + str(id)
        self.password = self.name + "_secret"
        
    def __repr__(self):
        return "%d/%s/%s" % (self.id, self.name, self.password)

# create some users with ids 1 to 20       
users = [User(id) for id in range(1, 21)]

# some protected url
@app.route('/')
@login_required
def home():
    return Response("Hello World!")

@app.route('/lista')
@login_required
def lista():
	aList = [];
	for entry in os.scandir(os.path.dirname(os.path.abspath(__file__))+'/Relatorios'):
		if entry.is_file():
			if entry.name[-4:] == '.zip':
				aList.append( entry.name);
	return Response(json.dumps(aList))

# somewhere to login
@app.route("/lista", methods=['GET','POST'])
def jsonx():
	aList = []
	if request.json: 
		myRelatorios = request.json
		username = myRelatorios.get("username");
		password = myRelatorios.get('password');
		for entry in os.scandir(os.path.dirname(os.path.abspath(__file__))+'/Relatorios'):
			if entry.is_file():
				if entry.name[-4:] == '.pdf' or entry.name[-4:] == '.xls':
					aList.append( entry.name);
	return Response(json.dumps(aList))


@app.route("/auth", methods=['GET','POST'])
@cross_origin()
# def verificaAuth(username,password):
def verificaAuth():
	# username = request.form['username']
	# password = request.form['password']
	username = request.json['username']
	password = request.json['password']
	# username = 'user'
	# password = 'pass'
	
	connection = pyodbc.connect(
		r'DRIVER={SQL Server};'
		r'SERVER={(LOCAL)\SQL2016};'
		r'DATABASE=appCandeias;'
		r'UID=sa;'
		r'PWD={nesher#2019}'
		)
		
	sql = """
	SELECT CASE WHEN EXISTS (
		SELECT [User] 
		FROM [appCandeias].[dbo].[ConfigCandeiasUsers] 
		WHERE [User] = '{0}' and [Pass] = '{1}'
	)
	THEN CAST(1 AS BIT)
	ELSE CAST(0 AS BIT) END
	as 'auth'
	""".format(username,password)
	
	cursor = connection.cursor().execute(sql)
	print(cursor)
	columns = [column[0] for column in cursor.description]

	results = []
	r = False
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))
			r = bool(row[0])

	# return Response(json.dumps(results, default=json_serial))
	return Response(json.dumps(r, default=json_serial))

@app.route("/cadastrousuario", methods=['GET','POST'])
@cross_origin()
# def verificaAuth(username,password):
def verificausuario():

	username = request.json['username']
	password = request.json['password']
	now = datetime.now().strftime("%Y-%d-%m")

	
	connection = pyodbc.connect(
		r'DRIVER={SQL Server};'
		r'SERVER={(LOCAL)\SQL2016};'
		r'DATABASE=appCandeias;'
		r'UID=sa;'
		r'PWD={nesher#2019}'
		)

	sql = """
		INSERT INTO ConfigCandeiasUsers (E3TimeStamp, [User], Pass, Token, User_Quality, Pass_Quality, Token_Quality) VALUES('{2}','{0}','{1}','{1}',,,)
		""".format(username,password,now)
	
	cursor = connection.cursor().execute(sql)
	connection.commit()
	return Response('passei')


@app.route("/ssl", methods=['GET','POST'])
@cross_origin()
def getSSLtestes():
	return Response(json.dumps("testes1235", default=json_serial))
    	



# somewhere to epm consulta
@app.route("/epmPotIndispGeral", methods=['GET','POST'])
@cross_origin()
def getepmPotIndispGeral():
#	web.header('Access-Control-Allow-Origin',      '*')
#	web.header('Access-Control-Allow-Credentials', 'true')
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	# sql = """SELECT MedPot.Timestamp as 'ts',  
	# 			CONVERT(float,MedPot.Value) as 'valor'
	# 			FROM EPM_Database.dbo.EpmQueryAggregateFunction(
	# 			-3
	# 			,DATEADD(d, -30, Convert(datetime, GetDate()))
	# 			,Convert(datetime, GetDate())
	# 			,3600000
	# 			,'Average' 
	# 			,'SE_BGI_14W1_Med_PotGI') MedPot"""

	sql= """
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(d, -30, Convert(smalldatetime, GetDate()))
                ,Convert(smalldatetime, GetDate())
                ,3600000
                ,'TimeAverage' 
                ,'CAN_CTRL_TotIndisp') MedPot
		"""
 
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]

	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))

	return Response(json.dumps(results, default=json_serial))


# somewhere to epm consulta
@app.route("/epmPotInefGeral", methods=['GET','POST'])
@cross_origin()
def getepmPotInefGeral():
#	web.header('Access-Control-Allow-Origin',      '*')
#	web.header('Access-Control-Allow-Credentials', 'true')
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	# sql = """SELECT MedPot.Timestamp as 'ts',  
	# 			CONVERT(float,MedPot.Value) as 'valor'
	# 			FROM EPM_Database.dbo.EpmQueryAggregateFunction(
	# 			-3
	# 			,DATEADD(d, -30, Convert(datetime, GetDate()))
	# 			,Convert(datetime, GetDate())
	# 			,3600000
	# 			,'Average' 
	# 			,'SE_BGI_14W1_Med_PotGI') MedPot"""

	sql= """
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(d, -30, Convert(smalldatetime, GetDate()))
                ,Convert(smalldatetime, GetDate())
                ,3600000
                ,'TimeAverage' 
                ,'CAN_CTRL_MetaPot') MedPot
		"""
 
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]

	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))

	return Response(json.dumps(results, default=json_serial))


@app.route("/epmPotOciosaGeral", methods=['GET','POST'])
@cross_origin()
def getepmPotOciosaGeral():
#	web.header('Access-Control-Allow-Origin',      '*')
#	web.header('Access-Control-Allow-Credentials', 'true')
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	# sql = """SELECT MedPot.Timestamp as 'ts',  
	# 			CONVERT(float,MedPot.Value) as 'valor'
	# 			FROM EPM_Database.dbo.EpmQueryAggregateFunction(
	# 			-3
	# 			,DATEADD(d, -30, Convert(datetime, GetDate()))
	# 			,Convert(datetime, GetDate())
	# 			,3600000
	# 			,'Average' 
	# 			,'SE_BGI_14W1_Med_PotGI') MedPot"""

	sql= """
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(d, -30, Convert(smalldatetime, GetDate()))
                ,Convert(smalldatetime, GetDate())
                ,3600000
                ,'TimeAverage' 
                ,'CAN_CTRL_PotOcsa') MedPot
		"""
 
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]

	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))

	return Response(json.dumps(results, default=json_serial))


# somewhere to epm consulta
@app.route("/epmMotor1", methods=['GET','POST'])
@cross_origin()
def getEPM1():
#	web.header('Access-Control-Allow-Origin',      '*')
#	web.header('Access-Control-Allow-Credentials', 'true')
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	# sql = """SELECT MedPot.Timestamp as 'ts',  
	# 			CONVERT(float,MedPot.Value) as 'valor'
	# 			FROM EPM_Database.dbo.EpmQueryAggregateFunction(
	# 			-3
	# 			,DATEADD(d, -30, Convert(datetime, GetDate()))
	# 			,Convert(datetime, GetDate())
	# 			,3600000
	# 			,'Average' 
	# 			,'SE_BGI_14W1_Med_PotGI') MedPot"""

	sql= """
			SELECT MedPot.Timestamp as 'ts',  
			CONVERT(float,MedPot.Value) as 'valor'
			FROM EPM_Database.dbo.EpmQueryAggregateFunction(
			-3
			,DATEADD(d, -30, Convert(smalldatetime, GetDate()))
			,DATEADD(HOUR, -1, Convert(smalldatetime, GetDate()))
			,3600000
			,'Average' 
			,'SE_BGI_14W1_Med_PotGI') MedPot
		"""
 
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]

	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))

	return Response(json.dumps(results, default=json_serial))


# somewhere to epm consulta
@app.route("/epmMotor2", methods=['GET','POST'])
@cross_origin()
def getEPM2():
#	web.header('Access-Control-Allow-Origin',      '*')
#	web.header('Access-Control-Allow-Credentials', 'true')
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql= """
			SELECT MedPot.Timestamp as 'ts',  
			CONVERT(float,MedPot.Value) as 'valor'
			FROM EPM_Database.dbo.EpmQueryAggregateFunction(
			-3
			,DATEADD(d, -30, Convert(smalldatetime, GetDate()))
			,DATEADD(HOUR, -1, Convert(smalldatetime, GetDate()))
			,3600000
			,'Average' 
			,'SE_BGII_14W2_Med_PotGII') MedPot
		"""

	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]

	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))

	return Response(json.dumps(results, default=json_serial))
	

# somewhere to epm consulta
@app.route("/epmMotorTotal", methods=['GET','POST'])
@cross_origin()
def getEPMPOTTOTAL():
#	web.header('Access-Control-Allow-Origin',      '*')
#	web.header('Access-Control-Allow-Credentials', 'true')
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql= """
				SELECT 
				Timestamp as ts,
				SUM(CONVERT(float,Value)) as valor
				FROM EPM_Database.dbo.EpmQueryAggregateFunction(
				-3
				,DATEADD(d, -30, Convert(smalldatetime, GetDate()))
				,DATEADD(HOUR, -1, Convert(smalldatetime, GetDate()))
				,3600000
				,'Average' 
				,'SE_BGI_14W1_Med_PotGI,SE_BGII_14W2_Med_PotGII')
				GROUP BY Timestamp
				order by Timestamp
		"""

	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]

	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))

	return Response(json.dumps(results, default=json_serial))
	


# somewhere to epm consulta
@app.route("/epmPotencia", methods=['GET','POST'])
@cross_origin()
def getEPMepmPotencia():
#	fator = request.json['fator']
	pathname = request.json['pathname']
#	web.header('Access-Control-Allow-Origin',      '*')
#	web.header('Access-Control-Allow-Credentials', 'true')
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	# sql = """SELECT MedPot.Timestamp as 'ts',  
	# 			CONVERT(float,MedPot.Value) as 'valor'
	# 			FROM EPM_Database.dbo.EpmQueryAggregateFunction(
	# 			-3
	# 			,DATEADD(d, -30, Convert(datetime, GetDate()))
	# 			,Convert(datetime, GetDate())
	# 			,3600000
	# 			,'Average' 
	# 			,'SE_BGI_14W1_Med_PotGI') MedPot"""

	sql= """
			SELECT MedPot.Timestamp as 'ts',  
			CONVERT(float,MedPot.Value) as 'valor'
			FROM EPM_Database.dbo.EpmQueryAggregateFunction(
			-3
			,DATEADD(d, -30, Convert(smalldatetime, GetDate()))
			,DATEADD(HOUR, -1, Convert(smalldatetime, GetDate()))
			,3600000
			,'Average' 
			,'{0}') MedPot
		""".format(pathname)
 
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]

	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))

	return Response(json.dumps(results, default=json_serial))



# somewhere to epm consulta
@app.route("/epmFator", methods=['GET','POST'])
@cross_origin()
def getFator():
#	web.header('Access-Control-Allow-Origin',      '*')
#	web.header('Access-Control-Allow-Credentials', 'true')
	fator = request.json['fator']
	pathname = request.json['pathname']

	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql= """
			SELECT 
				Timestamp as ts,
				SUM(CONVERT(float,Value)) /{0} as valor
				FROM EPM_Database.dbo.EpmQueryAggregateFunction(
				-3
				,DATEADD(d, -30, Convert(smalldatetime, GetDate()))
				,DATEADD(HOUR, -1, Convert(smalldatetime, GetDate()))
				,3600000
				,'Interpolative' 
				,'{1}')
				GROUP BY Timestamp
				order by Timestamp
		""".format(fator,pathname)

	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]

	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))

	return Response(json.dumps(results, default=json_serial))



# somewhere to epm consulta
@app.route("/epmHistPotenciaMotorEspecifico", methods=['GET','POST'])
@cross_origin()
def epmHistPotenciaMotorEspecifico():
	pathnameMotorEPM = request.json['motores']
	horas = request.json['horas']
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	
	sql="""
			SELECT Name as nome, MedPot.Timestamp as 'ts',  
			CONVERT(float,MedPot.Value) as 'valor'
			FROM EPM_Database.dbo.EpmQueryAggregateFunction(
			-3
			,DATEADD(HOUR, (-1)*{0}, Convert(datetime, GetDate()))
			,Convert(datetime, GetDate())
			,3600000
			,'TimeAverage' 
			,'{1}') MedPot
			order by ts, nome """.format(horas,pathnameMotorEPM)
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]
	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))
	return Response(json.dumps(results, default=json_serial))
	

# somewhere to epm consulta
@app.route("/epmMotor", methods=['GET','POST'])
@cross_origin()
def getEPMFlex():
	pathnameMotorEPM = request.json['motores']
	horas = request.json['horas']
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql = """SELECT MedPot.Timestamp as 'ts',  
				CONVERT(float,MedPot.Value) as 'valor'
				FROM EPM_Database.dbo.EpmQueryAggregateFunction(
				-3
				,DATEADD(h, (-1)*{0}, Convert(datetime, GetDate()))
				,Convert(datetime, GetDate())
				,3600000
				,'Average' 
				,'{1}') MedPot""".format(horas,pathnameMotorEPM)
				# ,'SE_BGI_14W1_Med_PotGI') MedPot"""
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]
	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))
	return Response(json.dumps(results, default=json_serial))
	

@app.route("/porcentagemEmZero/<path:enderecoEPM>/<path:tempo>", methods=['GET','POST'])
@cross_origin()
def porcentagemEmZero(enderecoEPM,tempo):
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql = 	"""
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(h, (-1)*{1}, Convert(datetime, GetDate()))
                ,Convert(datetime, GetDate())
                ,3600000*24
                ,'PercentInStateZero' 
                ,'{0}') MedPot
			""".format(enderecoEPM,tempo)
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]
	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))
	return Response(json.dumps(results, default=json_serial))


# INICIO GERAL
@app.route("/epmIndispGeral", methods=['GET','POST'])
@cross_origin()
def getEPMIndsG():
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql = 	"""	
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(d, -30, Convert(datetime, GetDate()))
                ,Convert(datetime, GetDate())
                ,3600000
                ,'TimeAverage' 
                ,'CAN_Contadores_Indisp') MedPot
			"""
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]
	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))
	return Response(json.dumps(results, default=json_serial))

@app.route("/epmOciososGeral", methods=['GET','POST'])
@cross_origin()
def getEPMIndsGepmOciososGeral():
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql = 	"""	
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(d, -30, Convert(datetime, GetDate()))
                ,Convert(datetime, GetDate())
                ,3600000
                ,'TimeAverage' 
                ,'CAN_Contadores_Ocsa') MedPot
			"""
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]
	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))
	return Response(json.dumps(results, default=json_serial))


@app.route("/epmProdGeral", methods=['GET','POST'])
@cross_origin()
def epmProdGeral():
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql = 	"""	
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(d, -30, Convert(datetime, GetDate()))
                ,Convert(datetime, GetDate())
                ,3600000
                ,'TimeAverage' 
                ,'CAN_Contadores_Prod') MedPot
			"""
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]
	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))
	return Response(json.dumps(results, default=json_serial))

@app.route("/epmRestGeral", methods=['GET','POST'])
@cross_origin()
def CAN_Contadores_Rest():
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql = 	"""	
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(d, -30, Convert(datetime, GetDate()))
                ,Convert(datetime, GetDate())
                ,3600000
                ,'TimeAverage' 
                ,'CAN_Contadores_Rest') MedPot
			"""
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]

	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))
	return Response(json.dumps(results, default=json_serial))


@app.route("/epmTotalGeral", methods=['GET','POST'])
@cross_origin()
def CAN_Contadores_Total():
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql = 	"""	
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(d, -30, Convert(datetime, GetDate()))
                ,Convert(datetime, GetDate())
                ,3600000
                ,'TimeAverage' 
                ,'CAN_Contadores_Total') MedPot
			"""
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]
	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))
	return Response(json.dumps(results, default=json_serial))
# FIM GERAL


# INICIO G1
@app.route("/epmIndispG1", methods=['GET','POST'])
@cross_origin()
def getEPMIndsG1():
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql = 	"""	
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(d, -30, Convert(datetime, GetDate()))
                ,Convert(datetime, GetDate())
                ,3600000
                ,'TimeAverage' 
                ,'CAN_GI_Contadores_Indisp') MedPot
			"""
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]
	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))
	return Response(json.dumps(results, default=json_serial))

@app.route("/epmProdG1", methods=['GET','POST'])
@cross_origin()
def epmProdGeral1():
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql = 	"""	
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(d, -30, Convert(datetime, GetDate()))
                ,Convert(datetime, GetDate())
                ,3600000
                ,'TimeAverage' 
                ,'CAN_GI_Contadores_Prod') MedPot
			"""
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]
	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))
	return Response(json.dumps(results, default=json_serial))


@app.route("/epmOciososG1", methods=['GET','POST'])
@cross_origin()
def epmOcGeral1():
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql = 	"""	
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(d, -30, Convert(datetime, GetDate()))
                ,Convert(datetime, GetDate())
                ,3600000
                ,'TimeAverage' 
                ,'CAN_GI_Contadores_Ocsa') MedPot
			"""
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]
	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))
	return Response(json.dumps(results, default=json_serial))


@app.route("/epmOciososG2", methods=['GET','POST'])
@cross_origin()
def epmOcGeral2():
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql = 	"""	
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(d, -30, Convert(datetime, GetDate()))
                ,Convert(datetime, GetDate())
                ,3600000
                ,'TimeAverage' 
                ,'CAN_GII_Contadores_Ocsa') MedPot
			"""
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]
	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))
	return Response(json.dumps(results, default=json_serial))

@app.route("/epmRestG1", methods=['GET','POST'])
@cross_origin()
def CAN_Contadores_Rest1():
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql = 	"""	
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(d, -30, Convert(datetime, GetDate()))
                ,Convert(datetime, GetDate())
                ,3600000
                ,'TimeAverage' 
                ,'CAN_GI_Contadores_Rest') MedPot
			"""
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]

	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))
	return Response(json.dumps(results, default=json_serial))


@app.route("/epmTotalG1", methods=['GET','POST'])
@cross_origin()
def CAN_Contadores_Total1():
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql = 	"""	
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(d, -30, Convert(datetime, GetDate()))
                ,Convert(datetime, GetDate())
                ,3600000
                ,'TimeAverage' 
                ,'CAN_GI_Contadores_Total') MedPot
			"""
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]
	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))
	return Response(json.dumps(results, default=json_serial))
# FIM G1


# INICIO G2
@app.route("/epmIndispG2", methods=['GET','POST'])
@cross_origin()
def getEPMIndsG2():
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql = 	"""	
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(d, -30, Convert(datetime, GetDate()))
                ,Convert(datetime, GetDate())
                ,3600000
                ,'TimeAverage' 
                ,'CAN_GII_Contadores_Indisp') MedPot
			"""
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]
	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))
	return Response(json.dumps(results, default=json_serial))

@app.route("/epmProdG2", methods=['GET','POST'])
@cross_origin()
def epmProdGeral2():
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql = 	"""	
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(d, -30, Convert(datetime, GetDate()))
                ,Convert(datetime, GetDate())
                ,3600000
                ,'TimeAverage' 
                ,'CAN_GII_Contadores_Prod') MedPot
			"""
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]
	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))
	return Response(json.dumps(results, default=json_serial))

@app.route("/epmRestG2", methods=['GET','POST'])
@cross_origin()
def CAN_Contadores_Rest2():
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql = 	"""	
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(d, -30, Convert(datetime, GetDate()))
                ,Convert(datetime, GetDate())
                ,3600000
                ,'TimeAverage' 
                ,'CAN_GII_Contadores_Rest') MedPot
			"""
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]

	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))
	return Response(json.dumps(results, default=json_serial))


@app.route("/epmTotalG2", methods=['GET','POST'])
@cross_origin()
def CAN_Contadores_Total2():
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql = 	"""	
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(d, -30, Convert(datetime, GetDate()))
                ,Convert(datetime, GetDate())
                ,3600000
                ,'TimeAverage' 
                ,'CAN_GII_Contadores_Total') MedPot
			"""
	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]
	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))
	return Response(json.dumps(results, default=json_serial))
# FIM G2


# somewhere to epm consulta
@app.route("/epmInef", methods=['GET','POST'])
@cross_origin()
def CAN_CTRL_MetaPot_Value():
#	web.header('Access-Control-Allow-Origin',      '*')
#	web.header('Access-Control-Allow-Credentials', 'true')
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql = 	"""	
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(d, -30, Convert(datetime, GetDate()))
                ,Convert(datetime, GetDate())
                ,3600000
                ,'TimeAverage' 
                ,'CAN_CTRL_MetaPot') MedPot
			"""

	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]

	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))

	return Response(json.dumps(results, default=json_serial))


# somewhere to epm consulta
@app.route("/epmPotAtTot", methods=['GET','POST'])
@cross_origin()
def CAN_CTRL_PotAtTot_Value():
#	web.header('Access-Control-Allow-Origin',      '*')
#	web.header('Access-Control-Allow-Credentials', 'true')
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql = 	"""	
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(d, -1, Convert(datetime, GetDate()))
                ,Convert(datetime, GetDate())
                ,3600000
                ,'TimeAverage' 
                ,'CAN_CTRL_PotAtTot') MedPot
			"""

	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]

	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))

	return Response(json.dumps(results, default=json_serial))


# somewhere to epm consulta
@app.route("/epmIndisp", methods=['GET','POST'])
@cross_origin()
def CAN_CTRL_TotIndisp_Value():
#	web.header('Access-Control-Allow-Origin',      '*')
#	web.header('Access-Control-Allow-Credentials', 'true')
	connection = pyodbc.connect(
			r'DRIVER={SQL Server};'
			r'SERVER={ELIPSESQLSERVER\ELIPSESQLSERVER};'
			r'DATABASE=EPM_Database;'
			r'UID=sa;'
			r'PWD=Elipse1'
			)
	sql = 	"""	
				SELECT MedPot.Timestamp as 'ts', 
                CONVERT(float,MedPot.Value) as 'valor'
                FROM EPM_Database.dbo.EpmQueryAggregateFunction(
                -3
                ,DATEADD(d, -1, Convert(datetime, GetDate()))
                ,Convert(datetime, GetDate())
                ,3600000
                ,'TimeAverage' 
                ,'CAN_CTRL_TotIndisp') MedPot
			"""

	cursor = connection.cursor().execute(sql)
	columns = [column[0] for column in cursor.description]

	results = []
	for row in cursor.fetchall():
			results.append(dict(zip(columns, row)))

	return Response(json.dumps(results, default=json_serial))



# somewhere to download
@app.route('/Relatorios2/<path:filename>')
@login_required
def d1(filename):
	#try:
		#filename = filename.replace(" ","")
		print(send_file(os.path.dirname(os.path.abspath(__file__))+'\\Relatorios\\'+filename))
		return send_file(os.path.dirname(os.path.abspath(__file__))+'\\Relatorios\\'+filename)
	#except:
	#	return Response("Erro!")

	
	
# somewhere to download
@app.route('/Relatorios1/<path:filename>', methods=["GET", "POST"])
@cross_origin()
def jsonD(filename):
	#print(filename)
	aList = []
	if request.json: 
		myRelatorios = request.json
		username = myRelatorios.get("username");
		password = myRelatorios.get('password');
		try:
			#filename = filename.replace(" ","")
			print(send_file(os.path.dirname(os.path.abspath(__file__))+'\\Relatorios\\'+filename))
			return send_file(os.path.dirname(os.path.abspath(__file__))+'\\Relatorios\\'+filename)
		except:
			return Response("Erro!")

	
@app.route('/listaRelatorios')
@login_required
def listaRelatorios():
	aList = [];
	for entry in os.scandir(os.path.dirname(os.path.abspath(__file__))+'/Relatorios'):
		if entry.is_file():
			if entry.name[-4:] == '.pdf' or entry.name[-4:] == '.xls' :
				aList.append( entry.name);
	return Response(json.dumps(aList))
 


# somewhere to login path
@app.route('/login/<path:username>')
def login1(username):
	login_user(User(username))
	return Response(username)


# somewhere to login
@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']        
        if password == username + "_secret":
            id = username.split('user')[1]
            user = User(id)
            login_user(user)
            return redirect(request.args.get("next"))
        else:
            return abort(401)
    else:
        return Response('''
		<style>
			@import url(https://fonts.googleapis.com/css?family=Roboto:300);

			.login-page {
			  width: 360px;
			  padding: 8% 0 0;
			  margin: auto;
			}
			.form {
			  position: relative;
			  z-index: 1;
			  background: #FFFFFF;
			  max-width: 360px;
			  margin: 0 auto 100px;
			  padding: 45px;
			  text-align: center;
			  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
			}
			.form input {
			  font-family: "Roboto", sans-serif;
			  outline: 0;
			  background: #f2f2f2;
			  width: 100%;
			  border: 0;
			  margin: 0 0 15px;
			  padding: 15px;
			  font-size: 16px;
			  font-style: oblique;
			  font-weight: bold;
			  box-sizing: border-box;
			  font-size: 14px;
			}
			.form button {
			  font-family: "Roboto", sans-serif;
			  text-transform: uppercase;
			  outline: 0;
			  background: #aaaaaa;
			  width: 100%;
			  border: 0;
			  padding: 15px;
			  color: #FFFFFF;
			  font-size: 14px;
			  -webkit-transition: all 0.3 ease;
			  transition: all 0.3 ease;
			  cursor: pointer;
			}
			.form button:hover,.form button:active,.form button:focus {
			  background: #aaaaaa;
			}
			.form .message {
			  margin: 15px 0 0;
			  color: #b3b3b3;
			  font-size: 12px;
			}
			.form .message a {
			  color: #bbbbbb;
			  text-decoration: none;
			}
			.form .register-form {
			  display: none;
			}
			.container {
			  position: relative;
			  z-index: 1;
			  max-width: 300px;
			  margin: 0 auto;
			}
			.container:before, .container:after {
			  content: "";
			  display: block;
			  clear: both;
			}
			.container .info {
			  margin: 50px auto;
			  text-align: center;
			}
			.container .info h1 {
			  margin: 0 0 15px;
			  padding: 0;
			  font-size: 36px;
			  font-weight: 300;
			  color: #1a1a1a;
			}
			.container .info span {
			  color: #4d4d4d;
			  font-size: 12px;
			}
			.container .info span a {
			  color: #000000;
			  text-decoration: none;
			}
			.container .info span .fa {
			  color: #aaaaaa;
			}
			body {
			  background: #303030; /* fallback for old browsers */
			  background: -webkit-linear-gradient(right, #f2f2f2, #303030);
			  background: -moz-linear-gradient(right, #f2f2f2, #303030);
			  background: -o-linear-gradient(right, #f2f2f2, #303030);
			  background: linear-gradient(to left, #f2f2f2, #303030);
			  font-family: "Roboto", sans-serif;
			  -webkit-font-smoothing: antialiased;
			  -moz-osx-font-smoothing: grayscale;      
			}
			input:-webkit-autofill, textarea:-webkit-autofill, select:-webkit-autofill { 
				 background-color: #bbbbbb !important;
				 -webkit-box-shadow: 0 0 0px 1000px #bbbbbb inset;
			}
		</style>
		<div class="login-page">
			<div class="form">
			<form action="" method="post">
				<p>Usuário
				<p><input type=text name=username>
				<p>Senha
				<p><input type=password name=password>
				<p><input style="border-radius: 4px;background-color: #7ea8ff5c;font-size: 16px;color=aliceblue" type=submit value=Login>
			</form>
		  </div>
		</div>
        ''')

# somewhere to download
@app.route('/Relatorios/<path:filename>')
@login_required
def d(filename):
	#try:
		#filename = filename.replace(" ","")
		print(send_file(os.path.dirname(os.path.abspath(__file__))+'\\Relatorios\\'+filename))
		return send_file(os.path.dirname(os.path.abspath(__file__))+'\\Relatorios\\'+filename)
	#except:
	#	return Response("Erro!")

	

# somewhere to logout
@app.route("/logout")
@login_required
def logout():
    logout_user()
    return Response('<p>Logged out</p>')


# handle login failed
@app.errorhandler(401)
def page_not_found(e):
    return Response('<p>Login failed</p>')
    
    
# callback to reload the user object        
@login_manager.user_loader
def load_user(userid):
    return User(userid)
    
 
context = SSL.Context(SSL.SSLv23_METHOD)
cer = os.path.join(os.path.dirname(__file__), 'ssl/server.crt')
key = os.path.join(os.path.dirname(__file__), 'ssl/server.key')
 
if __name__ == "__main__":
	context = (cer, key)
	# app.run(host='0.0.0.0', port=81, threaded=True, ssl_context=context)
	app.run(host='localhost', port=81, threaded=True)
    #app.run()