pyinstaller --onefile --distpath ./ candeias.exe.spec

del /q "__pycache__\*"
FOR /D %%p IN ("__pycache__\*.*") DO rmdir "%%p" /s /q
rmdir "__pycache__"
del /q "build\*"
FOR /D %%p IN ("build\*.*") DO rmdir "%%p" /s /q
rmdir "build"